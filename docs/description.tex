\documentclass[10pt]{article}

\usepackage{fullpage}
\usepackage{amsmath}

\usepackage[all=normal,paragraphs=tight,bibliography=tight]{savetrees}

\title{Algorithm description}
\author{Marcin Pilipczuk}

\begin{document}

\maketitle

\section{The implemented algorithm}

The implemented algorithm is a hybrid made out of the following ingredients.

\subsection{Basic reduction rules}

First, some basic reduction rules are implemented.
\begin{enumerate}
\item Reduce loops, vertices of degree at most two.
\item Contract two adjacent undeletable vertices.
\item Add to solution a vertex adjacent to an undeletable vertex
with a multiple edge.
\item Last but not least, if the maximum degree of a yet undecided vertex
drops to three, a polynomial-time solution is invoked, based
on graphic matroid parity; see~\cite{KociumakaP14} for the reduction.
The augmenting path algorithm for graphic matroid parity of
Gabow and Stallmann is implemented~\cite{GabowS86}.
\end{enumerate}

\subsection{Basic branching}

The basic branching algorithm is a branch first on a double edge, if present,
and if not, on the highest degree yet undecided vertex.

As observed by Yixin Cao in 2014 (private communication, I think he never published it), branching on a highest-degree vertex (with the aforementioned reduction rules) gives $\binom{3k}{k} \mathrm{poly}(n)$ running time bound, roughly for the following reason.
The degrees of undecided vertices do not increase in the process. If one branches on vertex $v$ with current degree $d_v \geq 4$, on average the degree of $v$ needs
to drop below $2$ by the end of the process, and thus at least $2 \leq d_v/2$
neighbors needs to be deleted (again, on average). However, vertices deleted
later have degree at most $d_v$, and thus account only for at most $d_v$ neighbors.
Consequently, in the leaf of the branching tree, at least a third of vertices
that were branched on needed to be deleted, and the running time bound follows.

Clearly, branching on a double edge is a $2^k$-time branch.

\subsection{Initial solution}

A natural question is what is the parameter $k$ in the previous subsection. 
Instead of iterating for increasing values of $k$ being the bound on the solution size, I found it much more efficient to first find an approximate solution
using a simple heuristic, and then run just one branching, with $k$ being the size of the solution found by the heuristic.

The heuristic is: iteratively apply (most of) the reduction rules, and if impossible, take the highest-degree vertex into the solution.
On most test cases, it is off from the optimum solution by 10-20\%.

\subsection{Extra preprocessing}

A few extra preprocessing rules have been added.
\begin{enumerate}
\item A degree-3 vertex incident to a double edge can be made undeletable.
\item A simple reduction of false twins, if there are more than two of them.
\item Consider independently 2-connected components, with some extra care
if a cutvertex is still undecided.
That is, we consider 2-connected components in a bottom-up order. For a
given component, if the cutvertex towards the parent is undecided, we first
branch on it, and return a minimum solution preferably containing the said
cutvertex.

Since we process components in a bottom-up order, either
\begin{itemize}
\item in the ``do not delete the cutvertex'' branch the next branching step will be a standard one,
  and we can charge the branch on the cutvertex to the subsequent standard branching; or
\item there are some deleted vertices in the component due to already processed children components,
  and the parameter decreased already.
\end{itemize}
In both cases, the algorithm remains FPT.
\end{enumerate}

\subsection{Tree decompositions}

Furthermore, after initial preprocessing, the algorithm
invokes an implementation of the classic Robertson-Seymour approximation
algorithm for treewidth.
If the width of the computed decomposition is small (the optimal threshold
turned out to be around 20-25, see the code for the final threshold),
   a standard naive DP is invoked.

The DP keeps track (very roughly) of the memory usage during the computation,
and abandons the computation if it runs out of memory. 
The DP also keeps the list of states in a lazy fashion (i.e., keeps only
    states that were reached during computation), which speeds up
the computation significantly e.g. on planar or near-planar graphs.
Here, my guess is that the algorithm implicitly makes use of the Catalan
bound of~\cite{DornFT12}.

\section{Things I tried and abandoned}

There is also an implementation of the branching algorithm of Kociumaka and myself~\cite{KociumakaP14} in the class \texttt{lowdegsolver}.
Apparently, it seems to be outperformed by the maximum degree branching
on almost all tests.

\bibliographystyle{abbrv}
\bibliography{references}

\end{document}
