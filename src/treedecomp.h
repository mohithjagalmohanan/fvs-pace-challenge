/*
 Copyright 2016 Marcin Pilipczuk.

 This file is part of fvs_pace_challenge,
 an implementation of FPT algorithm for Feedback Vertex Set,
 a submission to track B of PACE Challenge 2016.

 fvs_pace_challenge is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 fvs_pace_challenge is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with fvs_pace_challenge.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef FVS_PACE_CHALLENGE_TREEDECOMP_H
#define FVS_PACE_CHALLENGE_TREEDECOMP_H

#include "graph.h"

//#define TD_DEBUG_PRINT_BAGS

// Memory limit for states, in 8-byte words
// Very approximate, so we have some slack.
// In any case, it runs out of time if the mem limit is larger
#define TD_MEM_LIMIT (1ULL << 26)
class TdOutOfMemory {};

/* DP stuff */
#define TD_COMPRESS_STATES
#define TD_MAX_STATE_SIZE 24

#ifdef TD_COMPRESS_STATES
#define TD_FIELD_SIZE 5
#define TD_FIELDS_PER_64 12
#define TD_FIELD_MASK (((uint64_t)1 << TD_FIELD_SIZE) - 1)
#define TD_64COUNT ((TD_MAX_STATE_SIZE + TD_FIELDS_PER_64 - 1) / TD_FIELDS_PER_64)
#else
#endif


struct dp_state{
#ifdef TD_COMPRESS_STATES
    uint64_t t[TD_64COUNT];
#else
    uint8_t t[TD_MAX_STATE_SIZE];
#endif

    dp_state();
    uint64_t get_leader(uint64_t a) const;
    void set_leader(uint64_t a, uint64_t b);
};

struct CmpDpState {
    bool operator()(const dp_state &a, const dp_state &b) const;
};

class statearray{
public:
    vector<dp_state> states;
    vector<vector<int> > best_sol;
    map<dp_state, uint64_t, CmpDpState> state2index;
};

/* Class itself */
class treedecomp {
protected:
// Border of a component threshold, after which abort computation
// Theoretically, setting to 3t+4 allows up to treewidth t.
// Currently set up to TD_BASE_ABANDON_THRESHOLD - 2*log_2(#vertices / 256)
#define TD_BASE_ABANDON_THRESHOLD 20
    int TD_ABANDON_THRESHOLD;

    graph &g;
    vector<int> active;
    vector<int> comps;
    int last_comp_id;
    vector<string> vertex_names;

    vector<int> in, out, vis;
    int iter_id;
    bool dfs_mincut(int v);
    vector<int> mincut(vector<int> &s, vector<int> &t);

    vector<int> best_cut;
    int best_cut_score;
    void find_best_separator(vector<int> &border, int pos, vector<int> &s, vector<int> &t);

    vector<vector<int> > td_comp, td_border, td_bag, td_children;

    void dfs_flood_comp(int v);

    int __choose_extension_vertex_arbitrarily(int comp_id);
    int __choose_extension_vertex_max_border_deg(int comp_id);
    int __choose_extension_vertex_border_replace(int comp_id, vector<int>& border);
    int choose_extension_vertex(int comp_id, vector<int>& border);

    // DP stuff
    bool dpstate_merge(dp_state &state, int n, uint64_t a, uint64_t b);
    bool compute_base_state(vector<int> &verts, uint64_t mask, dp_state &base_state, vector<int> &filtered_verts);
    uint64_t project_mask(vector<int> &verts, uint64_t mask, vector<int> &child_verts);
    bool join_states(dp_state &parent, int n, const dp_state &child, const dp_state &child_base_state, int m, const vector<int> &tsl);
    uint64_t update_dp_table(statearray &SA, const dp_state &state, vector<int> &sol1, vector<int> &sol2);
public:
    treedecomp(graph &_g);

    vector<int> nextbag(int comp_id);
    void splitcomp(int comp_id, vector<int> &bag);
    void set_vertex_names(vector<string> &vn);
    int approximate_width();

    vector<int> solve();
};


#endif //FVS_PACE_CHALLENGE_TREEDECOMP_H
