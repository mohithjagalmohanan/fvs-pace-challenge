/*
 Copyright 2016 Marcin Pilipczuk.

 This file is part of fvs_pace_challenge,
 an implementation of FPT algorithm for Feedback Vertex Set,
 a submission to track B of PACE Challenge 2016.

 fvs_pace_challenge is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 fvs_pace_challenge is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with fvs_pace_challenge.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <cstdio>
#include <iostream>
#include <set>
#include <boost/program_options.hpp>
using namespace std;

#include "commons.h"
#include "graph.h"
#include "solver.h"

namespace po = boost::program_options;

void parse_commandline_options(int argc, const char * const *argv){
    po::options_description desc("Supported options");
    desc.add_options()
            ("seed,s", po::value<unsigned>(), "rand seed")
            ("input-file,f", po::value<string>(), "file to read")
            ("help,h", "produce help message")
            ;
    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);

    if (vm.count("help")){
        cout << desc << endl;
        exit(0);
    }
    if (vm.count("seed"))
        rnd.seed(vm["seed"].as<unsigned>());
    if (vm.count("input-file"))
        if (!freopen(vm["input-file"].as<string>().c_str(), "r", stdin)){
            cout << "Error opening file: " << vm["input-file"].as<string>() << endl;
            exit(0);
        }
}

int main(int argc, const char * const *argv){
    parse_commandline_options(argc, argv);
    graph g;
    vector<string> vertex_names = g.read_input();
    solver s;
    s.set_vertex_names(vertex_names);
    s.set_approximate_solution_size_cap(g);
    s.solve(g);
    s.print_solution();
    return 0;
}
