/*
 Copyright 2016 Marcin Pilipczuk.

 This file is part of fvs_pace_challenge,
 an implementation of FPT algorithm for Feedback Vertex Set,
 a submission to track B of PACE Challenge 2016.

 fvs_pace_challenge is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 fvs_pace_challenge is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with fvs_pace_challenge.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <cstdio>
#include <cassert>
#include <deque>
#include "graph.h"

/* Constructor empty graph */
graph::graph(bool __suppress_deg2_undeletable, bool __split_on_deletable_cutvertex) : suppress_deg2_undeletable(__suppress_deg2_undeletable), split_on_deletable_cutvertex(__split_on_deletable_cutvertex), n(0) {}

/* Constructor of a single connected component */
graph::graph(graph &g, vector<int> &verts, int &entry) : suppress_deg2_undeletable(g.suppress_deg2_undeletable), split_on_deletable_cutvertex(g.split_on_deletable_cutvertex) {
    induced_subgraph(g, verts, entry);
}

void graph::induced_subgraph(graph &g, vector<int> &verts, int &entry) {
    vector<int> f_verts;
    for (__typeof(verts.begin()) it = verts.begin(); it != verts.end(); ++it)
        if (g.IS_ALIVE(*it))
            f_verts.push_back(*it);
    n = (int)f_verts.size();
    vector<int> f_vertsrev = vector<int>((unsigned long)g.n, -1);
    for (int i = 0; i < (int)f_verts.size(); ++i)
        f_vertsrev[f_verts[i]] = i;
    nei = vector<vector<int> >((unsigned long)n);
    nei_pos = vector<vector<int> >((unsigned long)n);
    nei_double = vector<vector<bool> >((unsigned long)n);
    double_degree = vector<int>((unsigned long)n, 0);
    vertex_status = vector<int>((unsigned long)n, 0);
    for (int v = 0; v < n; ++v) {
        int ov = f_verts[v];
        for (int j = 0; j < (int) g.nei[ov].size(); ++j) {
            int ou = g.nei[ov][j];
            int u = f_vertsrev[ou];
            if (v < u){ // make edge only from the smaller index side
                int pos_v = (int)nei[v].size();
                int pos_u = (int)nei[u].size();
                nei[v].push_back(u);
                nei[u].push_back(v);
                nei_pos[v].push_back(pos_u);
                nei_pos[u].push_back(pos_v);
                bool is_double = g.nei_double[ov][j];
                nei_double[v].push_back(is_double);
                nei_double[u].push_back(is_double);
                if (is_double){
                    double_degree[u]++;
                    double_degree[v]++;
                }
            }
        }
    }

    for (__typeof(g.branching_hints.begin()) it = g.branching_hints.begin(); it != g.branching_hints.end(); ++it)
        if (f_vertsrev[*it] >= 0)
            branching_hints.push_back(f_vertsrev[*it]);
    for (int i = 0; i < n; ++i){
        assert(g.IS_ALIVE(f_verts[i]));
        vertex_list.push_back(i);
        vertex_status[i] = (1 & g.vertex_status[f_verts[i]]) + (i << 1);
    }
    orig_names = f_verts;
    for (int i = 0; i < n; ++i)
        check_if_reducible(i);
    if (entry >= 0 && g.IS_DELETABLE(entry))
        branching_hints.push_front(f_vertsrev[entry]);
    else
        entry = -1;
}

/*** Reading input ***/

/* Changes string name to integer index. If new vertex, creates its datastructures. */
int graph::name2vertex(char *name, map<string, int> &names2vertices, vector<string> &vertex_names, vector<map<int, int> > &nei_maps){
    string str(name);
    if (names2vertices.count(str) == 0) {
        int id = (int) vertex_names.size();
        vertex_names.push_back(str);
        vertex_status.push_back(2 * (int)vertex_list.size());
        nei.push_back(vector<int>());
        nei_pos.push_back(vector<int>());
        nei_double.push_back(vector<bool>());
        nei_maps.push_back(map<int,int>());
        double_degree.push_back(0);
        vertex_list.push_back(id);
        names2vertices[str] = id;
    }
    return names2vertices[str];
}

/* Reads the input. */
vector<string> graph::read_input() {
    char line[10000], name1[10000], name2[10000];
    map<string, int> names2vertices;
    vector<string> vertex_names;
    vector<map<int, int> > nei_maps;
    while (fgets(line, 9999, stdin)) {
        int i = 0;
        while (line[i] == ' ' || line[i] == '\t') ++i;
        if (line[i] != '#') {
            sscanf(line + i, "%s %s", name1, name2);
            int a = name2vertex(name1, names2vertices, vertex_names, nei_maps);
            int b = name2vertex(name2, names2vertices, vertex_names, nei_maps);
            if (a == b){
                solution_queue.push_back(a);
            } else {
                if (nei_maps[a].count(b) > 0){
                    int pos = nei_maps[a][b];
                    if (!nei_double[a][pos]){
                        double_degree[a]++;
                        double_degree[b]++;
                    }
                    nei_double[a][pos] = true;
                    nei_double[b][nei_pos[a][pos]] = true;
                } else {
                    int pos_a = (int)nei[a].size();
                    int pos_b = (int)nei[b].size();
                    nei[a].push_back(b);
                    nei[b].push_back(a);
                    nei_pos[a].push_back(pos_b);
                    nei_pos[b].push_back(pos_a);
                    nei_double[a].push_back(false);
                    nei_double[b].push_back(false);
                    nei_maps[a][b] = pos_a;
                    nei_maps[b][a] = pos_b;
                }
            }
        }
    }
    n = (int) vertex_names.size();
    orig_names = vector<int>((unsigned long)n);
    for (int i = 0; i < n; ++i) {
        check_if_reducible(i);
        orig_names[i] = i;
    }
    return vertex_names;
}

/*** Basic operations on graph ***/

/* degree of a vertex */
int graph::degree(int v) {
    return (int) (nei[v].size() + double_degree[v]);
}

void graph::check_if_reducible(int v){
    int d = (int)nei[v].size(), dd = double_degree[v];
    if (d + dd <= 2 || (d == 2 && dd == 1))
        simplify_queue.push_back(v);
}

void graph::recompute_reducible() {
    for (__typeof(vertex_list.begin()) it = vertex_list.begin(); it != vertex_list.end(); ++it)
        check_if_reducible(*it);
}

void graph::check_double_edge(int v, int pos_v){
    if (nei_double[v][pos_v]){
        int u = nei[v][pos_v];
        if (IS_UNDELETABLE(u)) {
            solution_queue.push_back(v);
        } if (IS_UNDELETABLE(v)) {
            solution_queue.push_back(u);
        }
    }
}

/* Deletes v.pos_v edge from the point of view of the neighbor. */
void graph::delete_edge(int v, int pos_v) {
    int u = nei[v][pos_v];
    int pos_u = nei_pos[v][pos_v];
    if (nei_double[v][pos_v])
        double_degree[u]--;
    int last_pos = (int)nei[u].size() - 1;
    if (pos_u != last_pos){
        nei[u][pos_u] = nei[u][last_pos];
        nei_pos[u][pos_u] = nei_pos[u][last_pos];
        nei_double[u][pos_u] = nei_double[u][last_pos];
        nei_pos[nei[u][pos_u]][nei_pos[u][pos_u]] = pos_u;
    }
    nei[u].pop_back();
    nei_pos[u].pop_back();
    nei_double[u].pop_back();
    check_if_reducible(u);
}

/* Removes vertex from list of vertices and sets its status to new_status */
void graph::delete_vertex_from_list(int v, int new_status){
    assert(IS_ALIVE(v));
    int pos = vertex_status[v] >> 1, pos2 = (int)vertex_list.size()-1;
    if (pos < pos2){
        int w = vertex_list[pos2];
        vertex_status[w] -= 2 * (pos2 - pos);
        vertex_list[pos] = w;
    }
    vertex_list.pop_back();
    vertex_status[v] = new_status;
}

/* Delete a vertex, without any FVS semantics. */
void graph::delete_vertex(int v, int new_status) {
    for (int i = 0; i < (int)nei[v].size(); ++i)
        delete_edge(v, i);
    nei[v].clear();
    nei_pos[v].clear();
    nei_double[v].clear();
    double_degree[v] = 0;
    delete_vertex_from_list(v, new_status);
}

/* Merge u onto v. Assumes deg(u) = 2.  */
void graph::merge_deg2_vertex(int v, int u) {
    assert(degree(u) == 2);
    assert(IS_UNDELETABLE(u));
    if ((int)nei[u].size() == 1){ // double edge to the same guy.
        assert(nei_double[u][0]);
        solution_queue.push_back(v);
        delete_vertex(u, STATUS_REMOVED);
    } else { // different neighbors
        assert(nei[u].size() == 2);
        assert(!nei_double[u][0]);
        assert(!nei_double[u][1]);
        int w_idx = nei[u][0] == v ? 1 : 0;
        int w = nei[u][w_idx];
        int i = 0;
        while (i < (int) nei[v].size() && nei[v][i] != w)
            ++i;
        if (i < (int) nei[v].size()) {
            if (!nei_double[v][i]) {
                double_degree[v]++;
                double_degree[w]++;
            }
            nei_double[v][i] = true;
            nei_double[w][nei_pos[v][i]] = true;
            check_double_edge(v, i);
            delete_edge(u, 0);
            delete_edge(u, 1);
        } else {
            int pos_v = nei_pos[u][1 - w_idx];
            int pos_w = nei_pos[u][w_idx];
            nei[v][pos_v] = w;
            nei[w][pos_w] = v;
            nei_pos[v][pos_v] = pos_w;
            nei_pos[w][pos_w] = pos_v;
            nei_double[v][pos_v] = false;
            nei_double[w][pos_w] = false;
        }
        nei[u].clear();
        nei_pos[u].clear();
        nei_double[u].clear();
        double_degree[u] = 0;
        delete_vertex_from_list(u, STATUS_REMOVED);
        check_if_reducible(v);
        check_if_reducible(w);
    }
}

/* Proclaim a vertex undeletable */
void graph::proclaim_undeletable(int v) {
    if (IS_DELETABLE(v)) {
        vertex_status[v]++;
        vector<int> undel_neis;
        for (int i = 0; i < (int)nei[v].size(); ++i) {
            if (nei_double[v][i]){
                assert(IS_DELETABLE(nei[v][i]));
                solution_queue.push_back(nei[v][i]);
            } else if (IS_UNDELETABLE(nei[v][i])) {
                undel_neis.push_back(nei[v][i]);
            }
        }
        if (undel_neis.size() > 0) {
            vector<int> list_neis;
            for (int i = 0; i < (int)nei[v].size(); ++i)
                if (IS_DELETABLE(nei[v][i])) {
                    if (!nei_double[v][i])
                        list_neis.push_back(nei[v][i]);
                    delete_edge(v, i);
                }
            for (__typeof(undel_neis.begin()) it = undel_neis.begin(); it != undel_neis.end(); ++it) {
                for (int j = 0; j < (int)nei[*it].size(); ++j)
                    if (nei[*it][j] != v) {
                        list_neis.push_back(nei[*it][j]);
                        delete_edge(*it, j);
                    }
                nei[*it].clear();
                nei_pos[*it].clear();
                nei_double[*it].clear();
                double_degree[*it] = 0;
                delete_vertex_from_list(*it, STATUS_REMOVED);
            }
            sort(list_neis.begin(), list_neis.end());
            __typeof(list_neis.begin()) it = list_neis.begin();
            int idx = 0;
            vector<int> &new_nei = nei[v], &new_pos = nei_pos[v];
            vector<bool> &new_double = nei_double[v];
            int &new_degree = double_degree[v];
            new_degree = 0;
            new_nei.clear(); new_pos.clear(); new_double.clear();
            while (it != list_neis.end()){
                int u = *it;
                int cnt = 0;
                new_nei.push_back(u);
                while (it != list_neis.end() && *it == u) {
                    cnt++;
                    it++;
                }
                new_double.push_back(cnt > 1);
                new_pos.push_back((int)nei[u].size());
                nei[u].push_back(v);
                nei_pos[u].push_back(idx);
                nei_double[u].push_back(cnt > 1);
                if (cnt > 1) {
                    solution_queue.push_back(u);
                    double_degree[v]++;
                    double_degree[u]++;
                }
                idx++;
            }
        }
        check_if_reducible(v);
    }
}

/* Proclaim a vertex part of the solution */
void graph::proclaim_solution(int v) {
    if (IS_ALIVE(v)) {
        assert(IS_DELETABLE(v));
        solution.push_back(v);
        delete_vertex(v, STATUS_SOLUTION);
    }
}

void graph::proclaim_solution(vector<int> &v){
    for (__typeof(v.begin()) it = v.begin(); it != v.end(); ++it)
        proclaim_solution(*it);
}

/*** Statistics ***/
void graph::print_stats(){
    fprintf(stderr, "Vertices: %d\n", (int)vertex_list.size());
#ifdef PRINT_STATS_DEGDIST
    print_stats_degdist();
#endif
}

void graph::print_stats_degdist(){
    fprintf(stderr, "  Deletable degrees:");
    vector<pair<int, int> > a;
    for (__typeof(vertex_list.begin()) it = vertex_list.begin(); it != vertex_list.end(); ++it)
        a.push_back(make_pair(degree(*it), *it));
    sort(a.begin(), a.end());
    for (__typeof(a.rbegin()) it = a.rbegin(); it != a.rend(); ++it)
        if (IS_DELETABLE(it->second))
            fprintf(stderr, " %d", it->first);
    fprintf(stderr, "\n  Undeletable degrees:");
    for (__typeof(a.rbegin()) it = a.rbegin(); it != a.rend(); ++it)
        if (IS_UNDELETABLE(it->second))
            fprintf(stderr, " %d", it->first);
    fprintf(stderr, "\n");
}

/*** Preprocessing ***/

const int DBL_OFFSET = 1000*1000;
vector<vector<uint64_t> > twins_sort_nei;
vector<uint64_t> twins_hash;

struct HashCmp {
    bool operator()(const int &a, const int &b) const {
        if (twins_hash[a] != twins_hash[b])
            return twins_hash[a] < twins_hash[b];
        if (twins_sort_nei[a].size() != twins_sort_nei[b].size())
            return twins_sort_nei[a].size() < twins_sort_nei[b].size();
        for (unsigned long i = 0; i < twins_sort_nei[a].size(); ++i)
            if (twins_sort_nei[a][i] != twins_sort_nei[b][i])
                return twins_sort_nei[a][i] < twins_sort_nei[b][i];
        return false;
    }
};

bool graph::check_false_twins() {
    bool modified = false;
    twins_sort_nei.resize(vertex_list.size());
    twins_hash.resize(vertex_list.size(), 0);
    vector<bool> tentative_undeletable((unsigned long)n, false);
    vector<int> list(vertex_list.size());
    for (unsigned long i = 0; i < vertex_list.size(); ++i) {
        list[i] = (int)i;
        int v = vertex_list[i];
        tentative_undeletable[v] = IS_UNDELETABLE(v);
        for (unsigned long j = 0; j < nei[v].size(); ++j)
            twins_sort_nei[i].push_back((uint64_t)(nei_double[v][j] ? (DBL_OFFSET + nei[v][j]) : nei[v][j]));
        sort(twins_sort_nei[i].begin(), twins_sort_nei[i].end());
        for (__typeof(twins_sort_nei[i].begin()) it = twins_sort_nei[i].begin(); it != twins_sort_nei[i].end(); ++it)
            twins_hash[i] = twins_hash[i] * 541 + *it; // mod 2^64
    }
    HashCmp cmp;
    sort(list.begin(), list.end(), cmp);
    unsigned long a = 0, b = 0;
    vector<int> new_undels;
    while (a < list.size()) {
        while (b < list.size() && !cmp(list[a], list[b]))
            b++;
        if (b == a + 1) {
            a = b;
            continue;
        }
        int v = vertex_list[list[a]];
        int undel_edges = 0;
        for (unsigned long i = 0; i < nei[v].size(); ++i)
            if (tentative_undeletable[nei[v][i]]) {
                undel_edges++;
                if (nei_double[v][i])
                    undel_edges++;
            }
        if (undel_edges >= 2) {
            // If there are at least two edges to undeletable neighbors, delete all but one vertex
            // Keep the undeletable one if present
            int kept = 0;
            while (a < b) {
                int w = vertex_list[list[a++]];
                if (IS_UNDELETABLE(w) || (kept == 0 && a == b)) // Keep the unique undeletable or the last one
                    kept++;
                else
                    proclaim_solution(w);
            }
            assert(kept == 1);
            modified = true;
        } else if (b - a >= nei[v].size()) {
            // Proclaim whole class undeletable. Reduce it to size 2.
            int counter = 0;
            while (a < b) {
                int w = vertex_list[list[a++]];
                if (counter >= 2 || IS_DELETABLE(w)) {
                    if (counter < 2) {
                        if (!tentative_undeletable[w]) {
                            tentative_undeletable[w] = true;
                            new_undels.push_back(w);
                        }
                    } else
                        delete_vertex(w, STATUS_REMOVED);
                    modified = true;
                }
                ++counter;
            }
        } else
            a = b;
    }
    for (__typeof(new_undels.begin()) it = new_undels.begin(); it != new_undels.end(); ++it)
        proclaim_undeletable(*it);
    twins_sort_nei.clear();
    twins_hash.clear();
    return modified;
}

void graph::simplify(){
    while(true){
        if (solution_queue.size() > 0) {
            int v = solution_queue.front();
            solution_queue.pop_front();
            if (IS_ALIVE(v))
                proclaim_solution(v);
        } else if (simplify_queue.size() > 0) {
            int v = simplify_queue.front();
            simplify_queue.pop_front();
            if (IS_ALIVE(v)) {
                int d = (int) nei[v].size(), dd = double_degree[v];
                if (d + dd <= 1)
                    delete_vertex(v, STATUS_REMOVED);
                else if (d + dd == 2) {
                    if (IS_UNDELETABLE(v)) {
                        if (suppress_deg2_undeletable)
                            merge_deg2_vertex(*nei[v].begin(), v);
                    } else {
                        proclaim_undeletable(v);
                        simplify_queue.push_back(v);
                    }
                } else if (d == 2 && dd == 1) {
                    proclaim_undeletable(v);
                }
            }
        } else if (!check_false_twins())
            break;
    }
#ifdef PRINT_STATS
    print_stats();
#endif
}

/* Is v a better branching vertex than u? */
bool graph::branching_compare(int v, int u){
    if (u == -1)
        return true;
    /* Number of double neighbors */
    if (double_degree[v] != double_degree[u])
        return double_degree[v] > double_degree[u];
    /* Degree */
    return nei[v].size() > nei[u].size();
}

int graph::__choose_branching_vertex_degree() {
    int v = -1;
    for (__typeof(vertex_list.begin()) it = vertex_list.begin(); it != vertex_list.end(); ++it)
        if (IS_DELETABLE(*it) && branching_compare(*it, v))
            v = *it;
    return v;
}

int graph::__choose_branching_vertex_IC() {
    vector<int> deg((unsigned long)n, -1);
    vector<int> queue;
    for (int i = 0; i < n; ++i)
        if (IS_DELETABLE(i)) {
            deg[i] = 0;
            for (int j = 0; j < (int)nei[i].size(); ++j)
                if (IS_DELETABLE(nei[i][j]))
                    deg[i]++;
            if (deg[i] <= 1)
                queue.push_back(i);
        }
    while (!queue.empty()){
        int v = queue.back();
        queue.pop_back();
        if (degree(v) > 3)
            return v;
        for (int j = 0; j < (int)nei[v].size(); ++j)
            if (IS_DELETABLE(nei[v][j]))
                if (--deg[nei[v][j]] == 1)
                    queue.push_back(nei[v][j]);
    }
    assert(false);
    return -1;
}

int graph::choose_branching_vertex(){
    if (vertex_list.size() == 0)
        return -1;
    while(branching_hints.size() > 0) {
        int v = branching_hints.front();
        branching_hints.pop_front();
        if (IS_DELETABLE(v))
            return v;
    }
    return __choose_branching_vertex_degree();
}

int graph::choose_branching_vertex_IC() {
    if (vertex_list.size() == 0)
        return -1;
    while(branching_hints.size() > 0) {
        int v = branching_hints.front();
        branching_hints.pop_front();
        if (IS_DELETABLE(v))
            return v;
    }
    return __choose_branching_vertex_IC();
}

int graph::max_degree(){
    int ret = 0;
    for (__typeof(vertex_list.begin()) it = vertex_list.begin(); it != vertex_list.end(); ++it)
        if (IS_DELETABLE(*it))
            ret = max(degree(*it), ret);
    return ret;
}

vector<int> graph::give_solution() {
    return translate_vertex_list(solution);
}

vector<int> graph::translate_vertex_list(vector<int> &l) {
    vector<int> ret;
    for(__typeof(l.begin()) it = l.begin(); it != l.end(); ++it)
        ret.push_back(orig_names[*it]);
    return ret;
}

int graph::components_dfs(int v){
    vis[v] = cnt++;
    low[v] = vis[v];
    stack.push_back(v);
    for (int i = 0; i < (int)nei[v].size(); ++i) {
        int u = nei[v][i];
        if (vis[u] <= 0){
            int me_idx = (int)stack.size();
            int res = components_dfs(u);
            if (res == vis[v] && (split_on_deletable_cutvertex || IS_UNDELETABLE(v))){
                // detach component
                ccomps.push_back(vector<int>(stack.begin() + me_idx, stack.end()));
                ccomps.back().push_back(v); // Entry point is always last.
                cc_entry.push_back(v);
                stack.resize(me_idx);
            }
            low[v] = min(low[v], res);
        } else {
            low[v] = min(low[v], vis[u]);
        }
    }
    return low[v];
}

void graph::components_run_one_dfs(int v) {
    int orig_size = (int)cc_entry.size();
    components_dfs(v);
    if ((int)stack.size() > 1) {
        ccomps.push_back(stack);
        cc_entry.push_back(-1);
    }
    stack.clear();
    // Clear entry of the last created component.
    if ((int)cc_entry.size() > orig_size)
        cc_entry[(int)cc_entry.size()-1] = -1;
}

int graph::components(){
    vis.clear(); vis.resize((unsigned)n, 0);
    low.clear(); low.resize((unsigned)n, INF);
    stack.clear();
    ccomps.clear();
    cc_entry.clear();
    cnt = 1;
    int main_comp = -1;
    if (!vertex_list.empty()) {
        uniform_int_distribution<> dist(0, (int)vertex_list.size()-1);
        components_run_one_dfs(vertex_list[dist(rnd)]);
        main_comp = (int)ccomps.size()-1;
    }
    for(__typeof(vertex_list.begin()) it = vertex_list.begin(); it != vertex_list.end(); ++it)
        if (vis[*it] <= 0)
            components_run_one_dfs(*it);
    return main_comp;
}

int graph::treewidth_dfs(int v, int depth, vector<int> &depths) {
    depths[v] = depth++;
    int res = 1;
    for (int i = 0; i < (int)nei[v].size(); ++i) {
        int w = nei[v][i];
        if (!depths[w])
            res = max(res, treewidth_dfs(w, depth, depths));
        else
            res = max(res, depth - depths[w]);
    }
    return res;
}

int graph::compute_dfs_treewidth() {
    vector<int> depths((unsigned long)n, 0);
    int res = 0;
    for (__typeof(vertex_list.begin()) it = vertex_list.begin(); it != vertex_list.end(); ++it)
        if (!depths[*it])
            res = max(res, treewidth_dfs(*it, 1, depths));
    return res;
}
    
void graph::print_edge_list() {
    for (__typeof(vertex_list.begin()) it = vertex_list.begin(); it != vertex_list.end(); ++it)
      for (int i = 0; i < (int)nei[*it].size(); ++i)
        if (*it < nei[*it][i] || nei_double[*it][i])
          printf("%d %d\n", *it, nei[*it][i]);
}
