/*
 Copyright 2016 Marcin Pilipczuk.

 This file is part of fvs_pace_challenge,
 an implementation of FPT algorithm for Feedback Vertex Set,
 a submission to track B of PACE Challenge 2016.

 fvs_pace_challenge is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 fvs_pace_challenge is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with fvs_pace_challenge.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <cstdio>
#include <algorithm>
#include <cassert>
#include <deque>
using namespace std;

#include "graph.h"
#include "deg3solver.h"
#include "solver.h"
#include "lowdegsolver.h"
#include "treedecomp.h"

// Maximum degree threshold, for which fall to lowdegsolver
// Setting below 4 disables lowdeg solver completely
// Currently disabled: it does not seem to solve low-degree tests any faster
#define MAX_DEG_FOR_LOWDEG_SOLVER 3

// Maximum width of the found decomposition when to solve by treedp
#define MAX_TREEWIDTH 24
#define TRY_TREEWIDTH

bool solver::reductions(graph &g) {
    /* Internal simplification rules for graph */
    g.simplify();
    /* If multiple connected components, split along them */
    int large = g.components();
    for (int i = 0; i < (int) g.ccomps.size(); ++i)
        if (i != large) {
            graph gc = graph(g, g.ccomps[i], g.cc_entry[i]);
            solver s;
            s.solution_size_cap = solution_size_cap - (int)g.solution.size();
            if (g.cc_entry[i] < 0)
                s.branching(gc);
            else {
                assert(!gc.branching_hints.empty());
                int v = gc.branching_hints.front();
                gc.branching_hints.pop_front();
                graph gc2 = gc;
                gc.proclaim_solution(v);
                s.branching(gc);
                gc2.proclaim_undeletable(v);
                s.branching(gc2);
            }
            if (s.solution_found)
                g.proclaim_solution(s.best_solution);
            else
                return true;
        }
    g.ccomps.clear(); g.cc_entry.clear();
    g.simplify();
    /* If cubic, invoke poly-time solver */
    int max_degree = g.max_degree();
    if (max_degree == 3){
        deg3solver deg3s(g);
        vector<int> ret = deg3s.solve();
        g.proclaim_solution(ret);
        g.simplify();
    } else if (max_degree >= 4 && max_degree <= MAX_DEG_FOR_LOWDEG_SOLVER){
        lowdegsolver lds;
        int dummy = -1;
        graph gc = graph(false, false);
        gc.induced_subgraph(g, g.vertex_list, dummy);
        lds.solution_size_cap = solution_size_cap - (int)g.solution.size();
        lds.solve(gc);
        if (lds.solution_found) {
            g.proclaim_solution(lds.best_solution);
            g.simplify();
        } else
            return true;
    }
    return false;
}

void solver::branching(graph &g){
    if (!reductions(g) && (int)g.solution.size() < solution_size_cap){
        int v = g.choose_branching_vertex();
        if (v == -1){
            // instance already solved
            solution_found = true;
            best_solution = g.give_solution();
            solution_size_cap = (int)best_solution.size();
        } else {
            graph second_copy = g;
            g.proclaim_solution(v);
            branching(g);
            second_copy.proclaim_undeletable(v);
            branching(second_copy);
        }
    }
}

void solver::iterative_compression(graph &g, vector<int> &apx_sol){
    vector<int> prev_sol = vector<int>();
    for (int i = (int)apx_sol.size() - 1; i >= (int)g.solution.size(); --i){
        graph copy = g;
        for (int j = (int)g.solution.size(); j < i; ++j)
            copy.delete_vertex(apx_sol[j], copy.STATUS_REMOVED);
        prev_sol.push_back(apx_sol[i]);
        copy.branching_hints = deque<int>(prev_sol.begin(), prev_sol.end());
        copy.recompute_reducible();
        solver s;
        s.branching(copy);
        prev_sol = vector<int>(s.best_solution.begin(), s.best_solution.end());
    }
    solution_found = true;
    g.proclaim_solution(prev_sol);
    best_solution = g.give_solution();
}

void solver::short_iterative_compression(graph &g, vector<int> &apx_sol){
    g.branching_hints = deque<int>(apx_sol.begin() + g.solution.size(), apx_sol.end());
    branching(g);
}

void solver::solve(graph &g){
    g.simplify();
#ifdef TRY_TREEWIDTH
    // Try tree decomposition
    bool treewidth_succeeded = false;
    try {
        treedecomp td(g);
        int width = td.approximate_width();
        if (width >= 0 && width <= MAX_TREEWIDTH) {
            // Naive tree decomposition DP
            //fprintf(stderr, "Trying tree dp, width=%d\n", width);
            vector<int> ret = td.solve();
            best_solution = g.give_solution();
            solution_found = true;
            best_solution.insert(best_solution.end(), ret.begin(), ret.end());
            treewidth_succeeded = true;
        }
    } catch(const TdOutOfMemory &e) {
        //fprintf(stderr, "   Tree DP aborted: out of memory.\n");
    }
    if (!treewidth_succeeded) {
        // Highest degree branching
        branching(g);
    }
#else
    branching(g);
#endif
}

vector<int> solver::greedy_approximation(graph g){
    g.simplify();
    int v;
    while ((v = g.choose_branching_vertex()) != -1){
        g.proclaim_solution(v);
        g.simplify();
    }
    return g.solution;
}

vector<int> solver::approximate(graph &g){
    return greedy_approximation(g);
}

void solver::set_approximate_solution_size_cap(graph &g) {
    vector<int> _apx = approximate(g);
    if (solution_size_cap > (int)_apx.size()) {
        solution_found = true;
        best_solution = g.translate_vertex_list(_apx);
        solution_size_cap = best_solution.size();
    }
}

void solver::print_solution(){
    assert(solution_found);
    for (__typeof(best_solution.begin()) it = best_solution.begin(); it != best_solution.end(); ++it)
        puts(vertex_names[*it].c_str());
}

void solver::set_vertex_names(vector<string> &vn){
    vertex_names = vn;
}

