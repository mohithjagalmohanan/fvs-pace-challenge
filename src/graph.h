/*
 Copyright 2016 Marcin Pilipczuk.

 This file is part of fvs_pace_challenge,
 an implementation of FPT algorithm for Feedback Vertex Set,
 a submission to track B of PACE Challenge 2016.

 fvs_pace_challenge is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 fvs_pace_challenge is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with fvs_pace_challenge.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef FVS_PACE_CHALLENGE_GRAPH_H
#define FVS_PACE_CHALLENGE_GRAPH_H

#include <algorithm>
#include <map>
#include <set>
#include <string>
#include <deque>
#include "commons.h"

using namespace std;

//#define BRANCHING_PREFER_UNDEL_NEI
//#define PRINT_STATS
//#define PRINT_STATS_DEGDIST

class graph {
public:
    const bool suppress_deg2_undeletable;
    const bool split_on_deletable_cutvertex;
    vector<vector<int> > nei, nei_pos;
    vector<vector<bool> > nei_double;
    vector<int> double_degree;
    vector<int> vertex_status;
    vector<int> solution;
    vector<int> vertex_list;
    deque<int> solution_queue, simplify_queue, branching_hints;
    int n;
    vector<int> orig_names;
    /* Populated upon components() call */
    vector<vector<int> > ccomps;
    vector<int> cc_entry;

    const int STATUS_SOLUTION = -1;
    const int STATUS_REMOVED = -2;

    inline bool IS_ALIVE(int v){ return vertex_status[v] >= 0; }
    inline bool IS_UNDELETABLE(int v) { return IS_ALIVE(v) && (vertex_status[v] & 1); }
    inline bool IS_DELETABLE(int v) { return IS_ALIVE(v) && !(vertex_status[v] & 1); }

    /* Constructor: empty graph */
    graph(bool __suppress_deg2_undeletable=true, bool __split_on_deletable_cutvertex=true);

    /* Constructor of a single connected component */
    graph(graph &g, vector<int> &verts, int &entry);
    void induced_subgraph(graph &g, vector<int> &verts, int &entry);

/*** Reading input ***/

    /* Reads the input. Returns array vertex id -> original vertex name */
    vector<string> read_input();

/*** Basic operations on graph ***/

    /* degree of a vertex */
    int degree(int v);

    /* Delete a vertex, without any FVS semantics. */
    void delete_vertex(int v, int new_status);

    /* Merge u onto v. Assumes deg(u) == 2. */
    void merge_deg2_vertex(int v, int u);

    /* Proclaim a vertex undeletable */
    void proclaim_undeletable(int v);

    /* Proclaim a vertex or a vector of vertices part of the solution */
    void proclaim_solution(int v);
    void proclaim_solution(vector<int> &v);

    /*** Statistics ***/
    void print_stats();
    void print_stats_degdist();

    /*** Preprocessing ***/
    void simplify();

    /* Populate ccomps: partition into connected components and 2-cc separated by undeletable cutvertices.
     * Returns the index of the component is started from. As it starts from a random vertex, it is likely a large one.*/
    int components();

    /* Choose vertex to branch, depending on the degree */
    int choose_branching_vertex();

    /* Choose vertex to branch, assuming IC step */
    int choose_branching_vertex_IC();

    void recompute_reducible();

    int max_degree();

    int compute_dfs_treewidth();

    bool check_false_twins();

    /*** Returning solution ***/
    vector<int> give_solution();
    /* Translate names from internal to original */
    vector<int> translate_vertex_list(vector<int> &l);

    /* Debug: print list of edges of current graph */
    void print_edge_list();


protected:
    const int INF = 1001*1001*1001;

    /* Arrays for components DFS and the DFS method itself */
    vector<int> vis, low, stack;
    int cnt;
    int components_dfs(int v);
    void components_run_one_dfs(int v);

    /* Deletes vertex from list of vertices, setting appropriate status */
    void delete_vertex_from_list(int v, int new_status);

    /* Deletes a single edge from one side */
    void delete_edge(int v, int pos_v);

    /* Checks if one can reduce an endpoint of a double edge */
    void check_double_edge(int v, int pos_v);

    /* Reading helper: Changes string name to integer index. If new vertex, creates its datastructures. */
    int name2vertex(char *name, map<string, int> &names2vertices, vector<string> &vertex_names, vector<map<int, int> > &nei_map);

    /* Check if needs to be put into simplification queue */
    void check_if_reducible(int v);

    /* Is v a better branching vertex than u? */
    bool branching_compare(int v, int u);

    /* DFS as a treewidth heuristic */
    int treewidth_dfs(int v, int depth, vector<int> &vis);

    /* Intermediate functions for choosing branching vertex */
    int __choose_branching_vertex_degree();
    int __choose_branching_vertex_IC();
};

#endif //FVS_PACE_CHALLENGE_GRAPH_H
